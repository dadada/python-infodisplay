from aiohttp import ClientSession
from asyncache import cached
from bs4 import BeautifulSoup
from cachetools import TTLCache, keys
from datetime import datetime, date, timedelta
from jinja2 import Template, Environment
from tornado.web import RequestHandler


class HoaHandler(RequestHandler):
    template: Template
    hoa_date: datetime.date

    def initialize(self, jinja: Environment, session: ClientSession) -> None:
        self.template = jinja.get_template("hoa.htm.jinja")
        self.session = session

    @cached(cache=TTLCache(maxsize=1, ttl=600), key=keys.methodkey)
    async def get_homepage(self) -> str:
        url = "https://hackenopenair.de/"
        async with self.session.get(url) as response:
            return await response.text()

    async def get_days_until_hoa(self) -> timedelta:
        today = date.today()

        homepage = await self.get_homepage()
        soup = BeautifulSoup(markup=homepage, features="html.parser")
        web_date, _, rest = soup.find("h3").contents[0].partition(" ")
        _, _, rest = rest.partition(" ")
        _, _, rest = rest.partition(".")
        _, _, rest = rest.partition(".")
        year, _, _ = rest.partition(" ")
        future = datetime.strptime(web_date + "." + year, "%d.%m.%Y")
        diff = future.date() - today

        return diff.days

    async def get(self):
        """
        Get days until HOA starts
        """

        days = await self.get_days_until_hoa()

        self.write(self.template.render(days=days))
