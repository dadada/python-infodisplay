from math import ceil
from os import environ

import arrow
import pygal
from aiohttp import ClientSession
from asyncache import cached
from cachetools import TTLCache, keys
from jinja2 import Template, Environment
from pygal.style import DarkSolarizedStyle
from tornado.web import RequestHandler


class DiagramHandler(RequestHandler):
    template: Template
    session: ClientSession
    headers: dict

    def initialize(self, jinja: Environment, session: ClientSession):
        self.template = jinja.get_template('diagram.htm.jinja')
        self.headers = {"Authorization": f"Bearer {environ.get('HOMEASSISTANT_TOKEN')}"}
        self.session = session

    @cached(cache=TTLCache(maxsize=2, ttl=55), key=keys.methodkey)
    async def get_sensor_data(self, sensor: str) -> (list, int):
        parameters = f"no_attributes&significant_changes_only&filter_entity_id=sensor.{sensor}"
        url = f"http://homeassistant.s0:8123/api/history/period?{parameters}"

        async with self.session.get(url, headers=self.headers) as response:
            json = await response.json()

        tuples = []
        maximum = 0
        for entity in json[0]:
            try:
                value = int(entity.get("state"))
                date = arrow.get(entity.get("last_changed"))
            except ValueError:
                continue

            maximum = max(maximum, value)
            tuples.append((date.datetime, value))

        return tuples, maximum

    @cached(cache=TTLCache(maxsize=1, ttl=55), key=keys.methodkey)
    async def make_svg(self, width: int, height: int) -> str:
        power, _ = await self.get_sensor_data("powespr0_power_meter_space")
        clients, maximum = await self.get_sensor_data("stratum0")

        # Override stroke width, see https://github.com/Kozea/pygal/issues/526
        style = DarkSolarizedStyle(stroke_width=2, background='transparent', plot_background='#073642')

        chart = pygal.DateTimeLine(
            human_readable=True,
            disable_xml_declaration=True,  # for embedded SVG use
            style=style,
            width=width,
            height=height,
            legend_at_bottom=True,
            legend_box_size=4,
            legend_at_bottom_columns=3,
            include_x_axis=True,  # always include 0 in Y range calculations
            show_y_guides=False,
            show_dots=False,
        )
        # Fix secondary Y scale
        chart.secondary_range = (0, ceil(maximum / 10) * 10)
        # Hours only for X scale
        chart.x_value_formatter = lambda d: d.strftime('%H:%M')

        # Actually add data to chart
        chart.add('Power consumption in W', power)
        chart.add('WiFi client count', clients, secondary=True)

        return chart.render()

    async def get(self):
        width = 800
        height = 480
        svg = await self.make_svg(width, height)

        self.write(self.template.render(chart=svg, width=width, height=height))
