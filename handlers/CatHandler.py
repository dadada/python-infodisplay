from aiohttp import ClientSession
from asyncache import cached
from cachetools import TTLCache, keys
from jinja2 import Template, Environment
from tornado.web import RequestHandler


class CatHandler(RequestHandler):
    template: Template
    session: ClientSession

    def initialize(self, jinja: Environment, session: ClientSession) -> None:
        self.template = jinja.get_template('cat.htm.jinja')
        self.session = session

    @cached(cache=TTLCache(maxsize=1, ttl=60), key=keys.methodkey)
    async def get_cat_url(self) -> dict:
        url = "https://cataas.com/cat?type=square&json=true"

        async with self.session.get(url) as response:
            return await response.json()

    async def get(self):
        """
        Get a cat
        """
        json = await self.get_cat_url()

        self.write(self.template.render(json=json))
