import arrow
from aiohttp import ClientSession
from jinja2 import Template, Environment
from tornado.web import RequestHandler


class BrandHandler(RequestHandler):
    template: Template
    session: ClientSession

    def initialize(self, jinja: Environment, session: ClientSession) -> None:
        self.template = jinja.get_template('brand.htm.jinja')
        self.session = session

    async def get_space_status(self) -> dict:
        url = "https://stratum0.org/status/status.json"

        async with self.session.get(url) as response:
            return (await response.json()).get("state")

    async def get(self):
        """
        Get the current space status
        """
        status = await self.get_space_status()
        time = arrow.Arrow.utcfromtimestamp(status.get("lastchange", arrow.utcnow().timestamp()))

        self.write(self.template.render(status=status, time=time.to("local")))
