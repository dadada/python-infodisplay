from os import environ

from aiohttp import ClientSession
from asyncache import cached
from cachetools import TTLCache, keys
from jinja2 import Template, Environment
from tornado.web import RequestHandler


class AirQualityHandler(RequestHandler):
    template: Template
    headers: dict
    session: ClientSession

    def initialize(self, jinja: Environment, session: ClientSession) -> None:
        self.template = jinja.get_template('airquality.htm.jinja')
        self.headers = {"Authorization": f"Bearer {environ.get('HOMEASSISTANT_TOKEN')}"}
        self.session = session

    @cached(cache=TTLCache(maxsize=3, ttl=60), key=keys.methodkey)
    async def get_sensor_data(self, sensor: str) -> dict:
        url = f"http://homeassistant.s0:8123/api/states/sensor.{sensor}"

        async with self.session.get(url, headers=self.headers) as response:
            return await response.json()

    async def get(self):
        """
        Get current air quality data
        """

        co2 = await self.get_sensor_data("air_sensor_co2")
        pm1 = await self.get_sensor_data("air_sensor_pm_1_0")
        pm25 = await self.get_sensor_data("air_sensor_pm_2_5")
        pm10 = await self.get_sensor_data("air_sensor_pm_10_0")
        temp = await self.get_sensor_data("air_sensor_temperature")

        self.write(self.template.render(co2=co2, pm1=pm1, pm25=pm25, pm10=pm10, temp=temp))
