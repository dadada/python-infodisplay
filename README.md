# python-infodisplay

## Getting started

Install required python libraries, e.g.

```
pip install -r requirements.txt
```

Set required environment variables in a sensible way.

```
# API access token for homeassistant.s0
HOMEASSISTANT_TOKEN=
# Matrix API token for chat.stratum0.org
MATRIX_TOKEN=
# If necessary, force IPv4-only connections
IP_MODE=legacy
```

Then, start the server and open http://localhost:8888

```
python3 index.py
```

## Structure of the application

Every widget on the infodisplay is rendered into Jinja templates by a handler residing in `handlers`. The `index.htm` uses HTMX to reload each widget independently on different timers. Handlers using external data sources use the `@cached` decorator to avoid frequent requests to their respective upstream services.
