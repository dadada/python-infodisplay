import asyncio
import datetime
import os
import socket

from aiohttp import ClientSession, ClientTimeout, TCPConnector
from jinja2 import Environment, FileSystemLoader, select_autoescape
from tornado.web import StaticFileHandler, Application

from handlers.AirQualityHandler import AirQualityHandler
from handlers.BrandHandler import BrandHandler
from handlers.CalendarHandler import CalendarHandler
from handlers.CatHandler import CatHandler
from handlers.ChatHandler import ChatHandler
from handlers.DiagramHandler import DiagramHandler
from handlers.HoaHandler import HoaHandler
from handlers.PublicTransportHandler import PublicTransportHandler
from handlers.WeatherHandler import WeatherHandler


def make_app():
    # Create jinja env for handlers to use
    env = Environment(
        loader=FileSystemLoader('templates', 'UTF-8'),
        autoescape=select_autoescape(['html', 'xml'])
    )
    connector = TCPConnector(family=socket.AF_INET) if os.environ.get('IP_MODE') == "legacy" else TCPConnector()
    session = ClientSession(timeout=ClientTimeout(total=20), connector=connector)

    settings = {"static_path": os.path.join(os.path.dirname(__file__), "static")}
    return Application(
        [
            (r"/airquality", AirQualityHandler, {"jinja": env, "session": session}),
            (r"/brand", BrandHandler, {"jinja": env, "session": session}),
            (r"/calendar", CalendarHandler, {"jinja": env, "session": session}),
            (r"/cat", CatHandler, {"jinja": env, "session": session}),
            (r"/chat", ChatHandler, {"jinja": env, "session": session}),
            (r"/diagram", DiagramHandler, {"jinja": env, "session": session}),
            (r"/hoa", HoaHandler, {"jinja": env, "session": session}),
            (r"/publictransport", PublicTransportHandler, {"jinja": env, "session": session}),
            (r"/weather", WeatherHandler, {"jinja": env, "station_id": "10348", "session": session}),
            # Serve everything else from /static/
            (r"/(.*)", StaticFileHandler, {"path": settings["static_path"], "default_filename": "index.htm"}),
        ],
        **settings
    )


async def main():
    app = make_app()
    app.listen(8888)
    await asyncio.Event().wait()


if __name__ == "__main__":
    asyncio.run(main())
